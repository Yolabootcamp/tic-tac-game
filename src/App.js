import "./App.css";
import Game from "./components/Game/Game";

function App() {
  return (
    <div className="App">
      <h1 className="app-title">Tic Tac Toe</h1>
      <Game />
    </div>
  );
}

export default App;
