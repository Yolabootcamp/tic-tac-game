import React from "react";
import "../Square/Square.css";

export default function Square({ value, onClick }) {
  const styles = value ? `squares ${value}` : `squares`;

  return (
    <div className='suqares-container'>
      <button className={styles} onClick={onClick}>
        {value}
      </button>
    </div>
  );
}
